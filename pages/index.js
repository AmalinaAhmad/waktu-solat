import Head from 'next/head'
import "tailwindcss/tailwind.css";
import axios from "axios"
import { useState, useEffect } from 'react';



export default function Home() {

  let [datas, setDatas] = useState(null);
  let [negeri, setNegeri] = useState("");
  let [zon, setZon] = useState("");
  let [waktuSolat, setWaktuSolat] = useState([]);

  useEffect(() => {
    axios.get('https://waktu-solat-api.herokuapp.com/api/v1/prayer_times.json', {
      params: {
        negeri: "selangor",
        zon: "kuala langat"
      }
    }).then(response => {
      setDatas(response.data.data);

      const { negeri = "", zon: [selectedZone] = {} } = response.data.data;

      const { nama: namaZon = "", waktu_solat = [] } = selectedZone;

      const waktuSolat = [];

      waktu_solat.map(value => {
        if (value.name !== "imsak" && value.name !== "syuruk" ){
          waktuSolat.push(value);
        }
      })

      setNegeri(negeri);
      setZon(namaZon);
      setWaktuSolat(waktuSolat);
    })
      .catch(err => {
        console.log(err)
      });
  }, [])

  return (


    <div className="flex bg-cover bg-center bg-hero-pattern rounded-b-besar h-screen">
      <div className="flex-1 p-6">
        <h1 className="font-arch mt-20 text-6xl text-colornew text-center">
          Waktu Solat
         <br>
          </br> Malaysia</h1>

        <h3 className="text-center mt-10 text-colorh2" >

          23 Januari 2021 Miladi | 10 Jamadilakhir 1442 Hijri

        </h3>

        <div className="flex justify-center mt-5">
          <button
            type="button"
            className=" bg-oren rounded-full font-roboto font-bold text-white px-10 py-1 transition duration-300 ease-in-out hover:bg-oren mr-6"
          >
            Lanjut
      </button>
        </div>
        <div className="grid grid-cols-5 mt-10">
          {waktuSolat.map((value, index) => {
            return (
              <div key={index}  className="flex-shrink-0 m-6 relative overflow-hidden bg-white rounded-lg max-w-xs shadow-lg">
                <svg className="absolute bottom-0 left-0 mb-8" viewBox="0 0 375 283" fill="none" style={{ transform: "scale(1.5)", opacity: "0.1" }}>
                  <rect x="159.52" y="175" width="152" height="152" rx="8" transform="rotate(-45 159.52 175)" fill="white" />
                  <rect y="107.48" width="152" height="152" rx="8" transform="rotate(-45 0 107.48)" fill="white" />
                </svg>
                <div className="relative pt-10 px-16 py-20 flex items-center justify-center">
                  <div className="block absolute w-48 h-48 bottom-0 left-0 -mb-24 ml-3" style={{ transform: "rotate3d(0, 0, 1, 20deg) scale3d(1, 0.6, 1)", opacity: "0.2" }}></div>
                  <h1 className="font-roboto text-4xl text-colorgrey text-center mt-4">
                    {value.time}
                    <h3 className="font-arch text-xs text-colorgrey text-center mt-3 capitalize">
                      {value.name}
                    </h3>
                  </h1>
                </div>
              </div>
            )
          })}

        </div>

      </div>
    </div>
  )
}
