module.exports = {
  purge: ['./pages/**/*.js', './components/**/*.js'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: theme => ({
       'hero-pattern': "url('/img/background.png')",
       'footer-texture': "url('/img/footer-texture.png')",
      }),
   

      fontFamily: {
        'sans': ['ui-sans-serif', 'system-ui'],
        'serif': ['ui-serif', 'Georgia'],
        'mono': ['ui-monospace', 'SFMono-Regular'],
        'display': ['Oswald'],
        'body': ['Open Sans'],
        'mon' : ['Montserrat'],
        'arch' : ['Archivo Black'],
        'roboto' : ['Roboto']
       },
       textColor: {
        'primary': '#3490dc',
        'secondary': '#ffed4a',
        'danger': '#e3342f',
        'colornew' : '#5D5959',
        'colorh2' : '#707070',
        'colorgrey' : '#3B3735',
      },
      borderRadius: {
       'besar': '10rem',
   
      },
      backgroundColor: theme => ({
       
        'oren': '#E76F51',
       })
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
